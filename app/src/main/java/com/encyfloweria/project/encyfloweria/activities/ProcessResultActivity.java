package com.encyfloweria.project.encyfloweria.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.encyfloweria.project.encyfloweria.Config;
import com.encyfloweria.project.encyfloweria.R;
import com.encyfloweria.project.encyfloweria.adapters.PlantResultDetailListAdapter;
import com.encyfloweria.project.encyfloweria.models.FlowerMetadataModel;
import com.encyfloweria.project.encyfloweria.models.HistoryModel;
import com.encyfloweria.project.encyfloweria.models.ImageMetadataModel;

public class ProcessResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_result);
        String pathToFile = getIntent().getExtras().getString("filePath");
        String matchImage = getIntent().getExtras().getString("matchImage");
        int matchPercentile = getIntent().getExtras().getInt("matchPercentile");

        ActiveAndroid.initialize(this);

        ListView detailListView = (ListView) findViewById(R.id.resultPlantDetailListView);
        ImageView capturedImageView = (ImageView) findViewById(R.id.resultImage);
        TextView percentileMatch = (TextView) findViewById(R.id.resultName);

        FlowerMetadataModel matchFlowerObject = null;
        for (ImageMetadataModel model : Config.IMAGE_API_MODEL.data) {
            if (model.flowerImagePath.equals(matchImage)) {
                int flowerID = Integer.parseInt(model.flowerId);
                for (FlowerMetadataModel flowerModel : Config.FLOWER_API_MODEL.data) {
                    if (Integer.parseInt(flowerModel.id) == flowerID) {
                        matchFlowerObject = flowerModel;
                        break;
                    }
                }
                break;
            }
        }

        //this is a dummy value
        if (matchFlowerObject != null) {
            //add to history
            HistoryModel newHistory = new HistoryModel();
            newHistory.flower_id = matchFlowerObject.id;
            newHistory.cap_image_path = pathToFile;
            newHistory.flower_percentile = String.format("%d", matchPercentile);
            newHistory.flower_name = matchFlowerObject.flowerName;
            newHistory.save();

            PlantResultDetailListAdapter adapter = new PlantResultDetailListAdapter(this, matchFlowerObject);
            detailListView.setAdapter(adapter);

            capturedImageView.setImageBitmap(decodeSampledBitmapFromResource(pathToFile, 256, 256));
            capturedImageView.invalidate();

            percentileMatch.setText(newHistory.flower_percentile + "% Match");
            percentileMatch.invalidate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_process_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Bitmap decodeSampledBitmapFromResource(String pathToFile, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathToFile, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathToFile, options);
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
