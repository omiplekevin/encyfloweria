package com.encyfloweria.project.encyfloweria;

import android.os.Environment;

import com.encyfloweria.project.encyfloweria.models.FlowerAPIModel;
import com.encyfloweria.project.encyfloweria.models.FlowerImageAPIModel;

/**
 * Created by OMIPLEKEVIN on Jan 09, 2016.
 */
public class Config {

    public static final String DIR_MAIN = Environment.getExternalStorageDirectory() + "/encyfloweria";

    public static final String DIR_IMAGE = DIR_MAIN + "/pictures";

    public static final String DIR_IMG_REFERENCE = DIR_MAIN + "/references";

    public static final String DIR_CACHE = DIR_MAIN + "/json";

    public static final String DIR_TEMP = DIR_MAIN + "/.temp";

    public static final String API_FLOWERS = "http://192.168.2.200/hcdc/public/v1/flower";

    public static final String API_IMAGES = "http://192.168.2.200/hcdc/public/v1/image";

    public static final String API_IMAGE_STORAGE = "http://192.168.2.200/hcdc/public/uploads";

    public static final String FILENAME_JSON_FLOWERS = "flowers.json";

    public static final String FILENAME_JSON_IMAGES = "images.json";

    public static double ORB_MAX_DIST = 0.0d;

    public static double ORB_MIN_DIST = 250.0d;

    //CONSTANT FIELDS
    public static FlowerAPIModel FLOWER_API_MODEL;

    public static FlowerImageAPIModel IMAGE_API_MODEL;

}
