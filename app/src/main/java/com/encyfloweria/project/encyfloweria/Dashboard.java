package com.encyfloweria.project.encyfloweria;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.encyfloweria.project.encyfloweria.activities.CameraActivity;
import com.encyfloweria.project.encyfloweria.activities.HistoryActivity;
import com.encyfloweria.project.encyfloweria.helpers.DownloadIntentService;
import com.encyfloweria.project.encyfloweria.helpers.Utilities;
import com.encyfloweria.project.encyfloweria.models.FlowerAPIModel;
import com.encyfloweria.project.encyfloweria.models.FlowerImageAPIModel;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.io.IOException;

public class Dashboard extends AppCompatActivity {

    private static final String TAG = "Dashboard";
    private BaseLoaderCallback mLoaderCallback;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initializeFields();
        initializeFolders();

        if (Utilities.isConnectedToNetwork(this)) {
            performAPIRequest();
        } else {
            try {
                String flowerJsonResponse = Utilities.readCacheFile(Config.DIR_CACHE + "/" + Config.FILENAME_JSON_FLOWERS);
                String flowerImageRelation = Utilities.readCacheFile(Config.DIR_CACHE + "/" + Config.FILENAME_JSON_IMAGES);
                Config.FLOWER_API_MODEL = new Gson().fromJson(flowerJsonResponse, FlowerAPIModel.class);
                Log.e(TAG, Config.FLOWER_API_MODEL.toString());
                Config.IMAGE_API_MODEL = new Gson().fromJson(flowerImageRelation, FlowerImageAPIModel.class);
                Log.e(TAG, Config.IMAGE_API_MODEL.toString());
            } catch (IOException e) {
                e.printStackTrace();
                AlertDialog errorDialog = new AlertDialog.Builder(this).create();
                errorDialog.setMessage("Error reading stored data, please connect to network and try again...");
                errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeFields(){
        mLoaderCallback = new BaseLoaderCallback(this) {
            @Override
            public void onManagerConnected(int status) {
                switch(status) {
                    case LoaderCallbackInterface.SUCCESS:
                        Log.e(TAG, "OpenCV Manager Connected");
                        break;
                    case LoaderCallbackInterface.INIT_FAILED:
                        Log.e(TAG, "Init Failed");
                        break;
                    case LoaderCallbackInterface.INSTALL_CANCELED:
                        Log.e(TAG, "Install Cancelled");
                        break;
                    case LoaderCallbackInterface.INCOMPATIBLE_MANAGER_VERSION:
                        Log.e(TAG, "Incompatible Version");
                        break;
                    case LoaderCallbackInterface.MARKET_ERROR:
                        Log.e(TAG, "Market Error");
                        break;
                    default:
                        Log.e(TAG, "OpenCV Manager Install");
                        super.onManagerConnected(status);
                        break;
                }
            }
        };
    }

    private void initializeFolders() {
        File mainFolder = new File(Config.DIR_MAIN);
        if (!mainFolder.mkdir()) {
            Log.e(TAG, "main folder is present");
        }

        File pictures = new File(Config.DIR_IMAGE);
        if (!pictures.mkdir()) {
            Log.e(TAG, "pictures is present");
        }

        File jsonCache = new File(Config.DIR_CACHE);
        if (!jsonCache.mkdir()) {
            Log.e(TAG, "cache is present");
        }

        File tempCache = new File(Config.DIR_TEMP);
        if (!tempCache.mkdir()) {
            Log.e(TAG, "temp is present");
        }

        File referenceFolder = new File(Config.DIR_IMG_REFERENCE);
        if (!referenceFolder.mkdir()) {
            Log.e(TAG, "referenceFolder is present");
        }

    }

    public void showHistory(View view) {
        Intent showHistory = new Intent(this, HistoryActivity.class);
        startActivity(showHistory);
    }

    public void getStarted(View view) {
        Intent getStartedCamera = new Intent(this, CameraActivity.class);
        startActivity(getStartedCamera);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    private void performAPIRequest() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Updating...");
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    boolean allSuccess = false;
                    String flowerJsonResponse = Utilities.sendHttpRequest(Config.API_FLOWERS, true, Config.FILENAME_JSON_FLOWERS, "GET", null);
                    Config.FLOWER_API_MODEL = new Gson().fromJson(flowerJsonResponse, FlowerAPIModel.class);
                    if (Config.FLOWER_API_MODEL.success) {
                        allSuccess = true;
                    } else {
                        allSuccess = false;
                    }
                    Log.e(TAG, Config.FLOWER_API_MODEL.toString());

                    String flowerImageRelation = Utilities.sendHttpRequest(Config.API_IMAGES, true, Config.FILENAME_JSON_IMAGES, "GET", null);
                    Config.IMAGE_API_MODEL = new Gson().fromJson(flowerImageRelation, FlowerImageAPIModel.class);
                    if (Config.IMAGE_API_MODEL.success) {
                        allSuccess = true;
                    } else {
                        allSuccess = false;
                    }
                    Log.e(TAG, Config.IMAGE_API_MODEL.toString());

                    if (allSuccess) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                startImageAssetDownload();
                            }
                        });
                    } else {
                        progressDialog.dismiss();
                        AlertDialog dialog = (new AlertDialog.Builder(Dashboard.this)).create();
                        dialog.setMessage("Something went wrong with the update API...\nTry again later.");
                        dialog.show();
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }
                        }
                    });
                }
            }
        }).start();
    }

    public void startImageAssetDownload() {
        final ProgressDialog downloadDialog = new ProgressDialog(this);
        downloadDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        downloadDialog.setMessage("Queuing downloads...");
        downloadDialog.show();

        Intent imageDownloadIntent = new Intent(this, DownloadIntentService.class);
        DownloadIntentService.setDownloadProgressCallback(new DownloadIntentService.DownloadProgressCallback() {
            @Override
            public void onCurrentDownloadItem(final String filename) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        downloadDialog.setMessage(filename);
                    }
                });
            }

            @Override
            public void onDownloadComplete() {
                // TODO: Jan 29, 2016 complete download here...
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        downloadDialog.dismiss();
                    }
                });
            }

            @Override
            public void onDownloadFailed(final String errMessage) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        downloadDialog.dismiss();
                        Toast.makeText(Dashboard.this, "" + errMessage, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        startService(imageDownloadIntent);
    }
}
