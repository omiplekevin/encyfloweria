package com.encyfloweria.project.encyfloweria.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class FlowerMetadataModel {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("flower_name")
    @Expose
    public String flowerName;
    @SerializedName("flower_description")
    @Expose
    public String flowerDescription;
    @SerializedName("flower_scientific_name")
    @Expose
    public String flowerScientificName;
    @SerializedName("flower_availabilty")
    @Expose
    public String flowerAvailabilty;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
