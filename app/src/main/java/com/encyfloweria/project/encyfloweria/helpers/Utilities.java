package com.encyfloweria.project.encyfloweria.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.encyfloweria.project.encyfloweria.Config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by OMIPLEKEVIN on Jan 29, 2016.
 */
public class Utilities {

    private static final String TAG = "Utilities";

    /**
     * checks connection status from Context passed as parameter
     * @param context context to check if connection is present
     * @return <code>true</code> if connected to or <code>false</code> otherwise
     */
    public static boolean isConnectedToNetwork(Context context){
        boolean isMobileData = false;
        boolean isWifi = false;
        boolean isEthernet = false;

        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        for(NetworkInfo networkInfo : networkInfos){
            if(networkInfo.getTypeName().equalsIgnoreCase("WIFI")){
                isWifi = networkInfo.isConnected();
            } else if(networkInfo.getTypeName().equalsIgnoreCase("MOBILE")){
                isMobileData = networkInfo.isConnected();
            } else if(networkInfo.getTypeName().equalsIgnoreCase("ETHERNET")){
                isEthernet = networkInfo.isConnected();
            }
        }

        return isMobileData || isWifi || isEthernet;
    }

    /**
     * Request Configuration from <i>configuration source</i>
     * @param sourceUrl source url where we can get our configuration
     * @param enableCacheConfig <code>true</code> if caching is enabled, <code>false</code> otherwise
     * @param fileName cache filename
     * @param method POST/GET
     * @param map Parameters for the POST method
     * @return <b>responseString</b> result of request
     */
    public static String sendHttpRequest(String sourceUrl, boolean enableCacheConfig, String fileName, String method, TreeMap<String, String> map){
        StringBuilder responseString = new StringBuilder();
        try {
            //throws MalformedURLException
            URL urlSource = new URL(sourceUrl);
            //throws IOException
            HttpURLConnection httpUrlConnection = (HttpURLConnection)urlSource.openConnection();
            httpUrlConnection.setRequestMethod(method.toUpperCase(Locale.getDefault()));
            httpUrlConnection.setConnectTimeout(10000);

            if (method.equals("POST")){
                StringBuilder params = new StringBuilder();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    params.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                }
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setInstanceFollowRedirects(false);
                httpUrlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                httpUrlConnection.setRequestProperty("charset", "utf-8");
                OutputStreamWriter writer = new OutputStreamWriter(httpUrlConnection.getOutputStream());
                writer.write(params.substring(0, params.length()-1));
                writer.flush();
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));
            String line;

            while((line = bufferedReader.readLine()) != null){
                responseString.append(line);
            }

            httpUrlConnection.getInputStream().close();

            if(enableCacheConfig && fileName != null){
                writeToCacheFile(responseString.toString(), fileName, false);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        return responseString.toString();
    }

    /**
     * This method will write passed content to specified file and requires append modes<br/><br/>
     * <i><b>Append modes</b></i><br/>
     * <code>true</code> content will be appended to the existing file, <code>false</code> otherwise
     * @param content content when writing <code>String</code> to the file
     * @param filename filename of the target file
     * @param enableAppend append mode, <code>true</code> or <code>false</code>
     * @return <code>true</code> if cache was written successfully, <code>false</code> otherwise
     */
    public static boolean writeToCacheFile(String content, String filename, boolean enableAppend) {
        try {
            if (enableAppend) {
                String cacheFileContent = readCacheFile(Config.DIR_CACHE + "/" + filename);
                Log.e(TAG, cacheFileContent);
                File cacheFile = new File(Config.DIR_CACHE + "/" + filename);
                if (TextUtils.isEmpty(cacheFileContent)){
                    if (!cacheFile.isFile() || !cacheFile.exists()){
                        if (cacheFile.createNewFile()){
                            Log.e(TAG, "created new file " + filename);
                        }
                    }
                } else {
                    content = "||" + content;
                }

                FileOutputStream fileOutputStream = new FileOutputStream(cacheFile, true);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.append(content);
                outputStreamWriter.close();
                fileOutputStream.close();

                Log.e(TAG, "CONTENT: " + readCacheFile(Config.DIR_CACHE + "/" + filename));
                return true;
            } else {
                File cacheFile = new File(Config.DIR_CACHE + "/" + filename);
                if (cacheFile.delete()) {
                    Log.e(TAG, "DELETE " + filename + " file");
                }
                FileOutputStream fileOutputStream = new FileOutputStream(cacheFile);
                fileOutputStream.write(content.getBytes());
                fileOutputStream.close();

                Log.e(TAG, "CONTENT: " + readCacheFile(Config.DIR_CACHE + "/" + filename));
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * read cache / precache file from storage directory
     * @param pathToFile source file of cached document
     * @return <b>String</b> content of the cache file
     */
    public static String readCacheFile(String pathToFile) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        File cacheFile = new File(pathToFile);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(cacheFile));
        String line;
        while((line = bufferedReader.readLine()) != null){
            stringBuilder.append(line);
        }

        bufferedReader.close();

        return stringBuilder.toString();

    }

    /**
     * transfer file to and from the specified path
     *
     * @param inputPath  from / source path
     * @param outputPath to / target path
     * @param method     <b>COPY</b> or <b>MOVE</b>
     */
    public static void transferFile(String fileName, String inputPath, String outputPath, String method) {
        try {
            FileInputStream inStream = new FileInputStream(inputPath + "/" + fileName);
            FileOutputStream outStream = new FileOutputStream(outputPath + "/" + fileName);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            inStream.close();
            outStream.close();

            // delete file from the temp_dir to avoid duplicate
            if (method.toUpperCase(Locale.getDefault()).equals("MOVE")) {
                File delFile = new File(inputPath + "/" + fileName);
                if (delFile.delete()) {
                    Log.e(TAG, "======= MOVED to " + outputPath + " =======");
                }
            } else {
                Log.e(TAG, "======= COPIED to " + outputPath + "=======");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
