package com.encyfloweria.project.encyfloweria.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ImageMetadataModel {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("flower_image_path")
    @Expose
    public String flowerImagePath;
    @SerializedName("flower_id")
    @Expose
    public String flowerId;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}