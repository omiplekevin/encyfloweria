package com.encyfloweria.project.encyfloweria.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OMIPLEKEVIN on Jan 29, 2016.
 */
public class FlowerAPIModel {

    @SerializedName("data")
    @Expose
    public List<FlowerMetadataModel> data = new ArrayList<>();

    @SerializedName("errors")
    @Expose
    public List<Object> errors = new ArrayList<Object>();

    @SerializedName("success")
    @Expose
    public Boolean success;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
