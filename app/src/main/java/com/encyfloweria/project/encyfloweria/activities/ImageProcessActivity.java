package com.encyfloweria.project.encyfloweria.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.encyfloweria.project.encyfloweria.Config;
import com.encyfloweria.project.encyfloweria.R;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.imgproc.Imgproc;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ImageProcessActivity extends AppCompatActivity {

    private Context context;
    private ProgressDialog progressDialog;
    private String fileUri;

    //    private List<ItemModel> finalMatches;
    /*private Mat bmpCap, bmpSD, img_scene, img_object, descriptor_object, descriptor_scene;
    private MatOfKeyPoint keypoints_object, keypoints_scene;
    private DescriptorMatcher matcher;
    private MatOfDMatch matches;
    private long lastProc;
    private long perImageProc;
    private long Totaltime;
    private long averageTimeExec;*/

    private final String TAG = "ImageProcessActivity";

    //task executors and constants
    private static ThreadPoolExecutor threadPoolExecutor;
    private final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    //testing only
    private static ConcurrentHashMap<String, Integer> processedImages;

    public ImageProcessActivity() {
        super();
    }

    public ImageProcessActivity(Context context, List<String> images) {
        this.context = context;
        processedImages = new ConcurrentHashMap<>();
        for (String image : images) {
            processedImages.put(image, -1);
        }
        if (threadPoolExecutor == null) {
            threadPoolExecutor = new ThreadPoolExecutor(
                    NUMBER_OF_CORES,
                    NUMBER_OF_CORES,
                    1,
                    TimeUnit.MINUTES,
                    new LinkedBlockingQueue<Runnable>());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_process);

        this.fileUri = getIntent().getExtras().getString("filePath");

        startProcessingImage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_process, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private void startProcessingImage() {
        final Bitmap bitmapCaptured = getBitmap(fileUri);
        for (final Map.Entry<String, Integer> entry : processedImages.entrySet()) {
            threadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //======================================================
                        //===========MAIN ALGORITHM=============================

                        /*Bitmap bitmapSD = getBitmap(Config.DIR_IMG_REFERENCE + "/" + entry.getKey());
                        bmpCap = new Mat();
                        bmpSD = new Mat();
                        img_object = new Mat();
                        img_scene = new Mat();

                        Utils.bitmapToMat(bitmapCaptured, bmpCap);
                        Utils.bitmapToMat(bitmapSD, bmpSD);
                        Imgproc.cvtColor(bmpCap, img_object, Imgproc.COLOR_RGB2GRAY);
                        Imgproc.cvtColor(bmpSD, img_scene, Imgproc.COLOR_RGB2GRAY);


                        FeatureDetector detector = FeatureDetector.create(FeatureDetector.ORB);

                        keypoints_object = new MatOfKeyPoint();
                        keypoints_scene  = new MatOfKeyPoint();

                        detector.detect(img_object, keypoints_object);
                        detector.detect(img_scene, keypoints_scene);

                        DescriptorExtractor extractor = DescriptorExtractor.create(FeatureDetector.ORB);

                        descriptor_object = new Mat();
                        descriptor_scene = new Mat();

                        extractor.compute(img_object, keypoints_object, descriptor_object);
                        extractor.compute(img_scene, keypoints_scene, descriptor_scene);

                        matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
                        matches = new MatOfDMatch();

                        if(descriptor_object.cols() == descriptor_object.cols()
                                && descriptor_object.type() == descriptor_scene.type()
                                && descriptor_object.cols() > 0
                                && descriptor_scene.cols() > 0) {
                                    matcher.match(descriptor_object, descriptor_scene, matches);
                                    List<DMatch> matchesList = matches.toList();

                                    Double max_dist = Config.ORB_MAX_DIST;
                                    Double min_dist = Config.ORB_MIN_DIST;

                                    for(int i = 0; i < descriptor_object.rows(); i++){
                                        Double dist = (double) matchesList.get(i).distance;
                                        if(dist < min_dist) min_dist = dist;
                                        if(dist > max_dist) max_dist = dist;
                                    }

                                    LinkedList<DMatch> good_matches = new LinkedList<DMatch>();
                                    MatOfDMatch gm = new MatOfDMatch();

                                    for(int i = 0; i < descriptor_object.rows(); i++){
                                        if(matchesList.get(i).distance < 2*min_dist){
                                            good_matches.addLast(matchesList.get(i));
                                        }
                                    }

                                    gm.fromList(good_matches);

                                    LinkedList<Point> objList = new LinkedList<Point>();
                                    LinkedList<Point> sceneList = new LinkedList<Point>();

                                    List<KeyPoint> keypoints_objectList = keypoints_object.toList();
                                    List<KeyPoint> keypoints_sceneList = keypoints_scene.toList();

                                    for(int i = 0; i<good_matches.size(); i++){
                                        objList.addLast(keypoints_objectList.get(good_matches.get(i).queryIdx).pt);
                                        sceneList.addLast(keypoints_sceneList.get(good_matches.get(i).trainIdx).pt);
                                    }

                                    MatOfPoint2f obj = new MatOfPoint2f();
                                    obj.fromList(objList);

                                    MatOfPoint2f scene = new MatOfPoint2f();
                                    scene.fromList(sceneList);

                                    Mat mask = new Mat();
                                    Calib3d.findHomography(obj, scene);

                                    int maskVal = 0;
                                    for(int i=0;i<mask.rows();i++)
                                    {
                                        for(int j=0;j<mask.cols();j++)
                                        {
                                            if((mask.get(i, j)[0] == 1))
                                            {
                                                ++maskVal;
                                            }
                                        }
                                    }
                                    long lap = (System.currentTimeMillis() - perImageProc);
                                    Log.e("RESULT LOG", entry.getKey() + " MATCH = " + (Config.ORB_MIN_DIST - maskVal) + ", "+ lap + "ms");
                                    updateConcurrentList(entry.getKey(), maskVal);
                        }*/

                        FeatureDetector detector = FeatureDetector.create(FeatureDetector.ORB);
                        DescriptorExtractor descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB);;
                        DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);

                        Bitmap bitmapSD = getBitmap(Config.DIR_IMG_REFERENCE + "/" + entry.getKey());

                        //first image
                        Mat img1 = new Mat();
                        Utils.bitmapToMat(bitmapCaptured, img1);
                        Mat descriptors1 = new Mat();
                        MatOfKeyPoint keypoints1 = new MatOfKeyPoint();

                        detector.detect(img1, keypoints1);
                        descriptor.compute(img1, keypoints1, descriptors1);

                        //second image
                        Mat img2 = new Mat();
                        Utils.bitmapToMat(bitmapSD, img2);
                        Mat descriptors2 = new Mat();
                        MatOfKeyPoint keypoints2 = new MatOfKeyPoint();

                        detector.detect(img2, keypoints2);
                        descriptor.compute(img2, keypoints2, descriptors2);

                        //matcher should include 2 different image's descriptors
                        MatOfDMatch  matches = new MatOfDMatch();
                        matcher.match(descriptors1, descriptors2, matches);

                        int match = matches.toArray().length;
                        int keypoints = keypoints2.toArray().length;
                        float percentile = ((float)match/(float)keypoints) * 100;

                        Log.e(TAG, match + " matches / " + keypoints + " keypoints " + Config.DIR_IMG_REFERENCE + "/" + entry.getKey());
                        updateConcurrentList(entry.getKey(), (int)percentile);
                        //feature and connection colors
                        /*Scalar RED = new Scalar(255,0,0);
                        Scalar GREEN = new Scalar(0,255,0);
                        //output image
                        Mat outputImg = new Mat();
                        MatOfByte drawnMatches = new MatOfByte();
                        //this will draw all matches, works fine
                        Features2d.drawMatches(img1, keypoints1, img2, keypoints2, matches,
                                outputImg, GREEN, RED,  drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);*/
                        //======================================================
                    } catch (final Exception e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ImageProcessActivity.this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
                                finish();
                            }
                        });
                    }
                }
            });
        }
    }

    private Bitmap getBitmap(String path)
    {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options(); // Limit the filesize since 5MP pictures will kill your RAM
        bitmapOptions.inSampleSize = 1;
        Bitmap bmp = BitmapFactory.decodeFile(path, bitmapOptions);

        //convert into ARGB_8888
        int MAX_DIM = 300;
        int w, h;
        if (bmp.getWidth() >= bmp.getHeight())
        {
            w = MAX_DIM;
            h = bmp.getHeight()*MAX_DIM/bmp.getWidth();
        }
        else
        {
            h = MAX_DIM;
            w = bmp.getWidth()*MAX_DIM/bmp.getHeight();
        }
        bmp = Bitmap.createScaledBitmap(bmp, w, h, false);
        Bitmap img1 = bmp.copy(Bitmap.Config.ARGB_8888, false);


        return img1;
    }

    /**
     * update and check the concurrent list for results
     *
     * @param entryKey   entry key of the item
     * @param entryValue new value for entry key
     */
    private void updateConcurrentList(final String entryKey, int entryValue) {
        Log.e(TAG, "updating rating of " + entryKey + " to: " + entryValue);
        int completeChecker = 0;
        processedImages.put(entryKey, entryValue);
        for (Map.Entry<String, Integer> entry : processedImages.entrySet()) {
            if (entry.getValue() == -1F) {
                break;
            }
            completeChecker++;
        }
        if (completeChecker == processedImages.size()) {
            Log.e(TAG, "Everything is done processing...");
            //get the highest percentage value
            String highKey = "";
            int highPercentile = 0;
            for (Map.Entry<String, Integer> entry : processedImages.entrySet()) {
                if (entry.getValue() < 100 && entry.getValue() > highPercentile) {
                    highPercentile = entry.getValue();
                    highKey = entry.getKey();
                }
            }
            if (highPercentile < 80) {
                highPercentile = 0;
                highKey = "";
            }
            final String HIGHKEY = highKey;
            final int HIGHPERCENTILE = highPercentile;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent resultIntent = new Intent(ImageProcessActivity.this, ProcessResultActivity.class);
                    resultIntent.putExtra("filePath", fileUri);
                    resultIntent.putExtra("matchImage", HIGHKEY);
                    resultIntent.putExtra("matchPercentile", HIGHPERCENTILE);
                    startActivity(resultIntent);
                    finish();
                }
            });
        }
    }
}
