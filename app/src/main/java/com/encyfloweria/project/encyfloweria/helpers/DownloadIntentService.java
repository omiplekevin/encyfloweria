package com.encyfloweria.project.encyfloweria.helpers;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.encyfloweria.project.encyfloweria.Config;
import com.encyfloweria.project.encyfloweria.models.ImageMetadataModel;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by OMIPLEKEVIN on Jan 29, 2016.
 */
public class DownloadIntentService extends IntentService {

    public final String TAG = "DownloadIntentService";
    public static DownloadProgressCallback downloadProgressCallback;

    public DownloadIntentService(){
        super("DownloadIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        performQueueDownload();
    }

    public static void setDownloadProgressCallback(DownloadProgressCallback downloadProgressCallback) {
        DownloadIntentService.downloadProgressCallback = downloadProgressCallback;
    }

    private void performQueueDownload(){
        for(ImageMetadataModel model : Config.IMAGE_API_MODEL.data) {
            final String sourceUrl = Config.API_IMAGE_STORAGE + "/" + model.flowerImagePath;
            Log.e(TAG, sourceUrl);
            try {
                //core download process
                downloadProgressCallback.onCurrentDownloadItem(model.flowerImagePath);
                URL sourceURL = new URL(sourceUrl);
                URLConnection urlConnection = sourceURL.openConnection();
                urlConnection.connect();

                InputStream inputStream = new BufferedInputStream(sourceURL.openStream(), 8192);
                OutputStream outputStream = new FileOutputStream(Config.DIR_TEMP + "/" + model.flowerImagePath);

                byte data[] = new byte[4096];
                int count;
                int currentTotal = 0;
                while ((count = inputStream.read(data)) != -1) {
                    currentTotal += count;
                    outputStream.write(data, 0, count);
                }

                outputStream.flush();
                outputStream.close();
                inputStream.close();

                Utilities.transferFile(model.flowerImagePath,
                        Config.DIR_TEMP,
                        Config.DIR_IMG_REFERENCE,
                        "MOVE");

            } catch (IOException e) {
                e.printStackTrace();
                downloadProgressCallback.onDownloadFailed(e.getMessage());
            }
        }
        downloadProgressCallback.onDownloadComplete();
    }

    public interface DownloadProgressCallback{
        void onCurrentDownloadItem(String filename);

        void onDownloadComplete();

        void onDownloadFailed(String errMessage);
    }
}
