package com.encyfloweria.project.encyfloweria.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.encyfloweria.project.encyfloweria.R;
import com.encyfloweria.project.encyfloweria.models.HistoryModel;

import java.util.List;

/**
 *
 * Created by OMIPLEKEVIN on Jan 09, 2016.
 */
public class HistoryListAdapter extends BaseAdapter {

    Context context;
    List<HistoryModel> objects;

    public HistoryListAdapter(Context context, List<HistoryModel> objects) {
        this.context = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return this.objects.size();
    }

    @Override
    public HistoryModel getItem(int position) {
        return this.objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.listview_history_listitem, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.plantImage = (ImageView) convertView.findViewById(R.id.imageHolder);
            viewHolder.plantName = (TextView) convertView.findViewById(R.id.plantName);
            viewHolder.plantDescription = (TextView) convertView.findViewById(R.id.plantDescription);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.plantImage.setImageBitmap(decodeSampledBitmapFromResource(this.objects.get(position).cap_image_path, 256, 256));
        viewHolder.plantName.setText(this.objects.get(position).flower_name);
        viewHolder.plantDescription.setText(this.objects.get(position).flower_percentile + "% Match");

        return convertView;
    }

    class ViewHolder{
        ImageView plantImage;
        TextView plantName;
        TextView plantDescription;
    }

    private Bitmap decodeSampledBitmapFromResource(String pathToFile, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathToFile, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathToFile, options);
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
