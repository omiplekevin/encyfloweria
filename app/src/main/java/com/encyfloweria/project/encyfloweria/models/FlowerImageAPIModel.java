package com.encyfloweria.project.encyfloweria.models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class FlowerImageAPIModel {

    @SerializedName("data")
    @Expose
    public List<ImageMetadataModel> data = new ArrayList<ImageMetadataModel>();
    @SerializedName("errors")
    @Expose
    public List<Object> errors = new ArrayList<Object>();
    @SerializedName("success")
    @Expose
    public Boolean success;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}