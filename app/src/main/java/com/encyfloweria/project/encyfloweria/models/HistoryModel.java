package com.encyfloweria.project.encyfloweria.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OMIPLEKEVIN on Jan 29, 2016.
 */
@Table(name = "History")
public class HistoryModel extends Model {

    @Column(name = "flower_id")
    public String flower_id;

    @Column(name = "flower_percentile")
    public String flower_percentile;

    @Column(name = "cap_image_path")
    public String cap_image_path;

    @Column(name = "flower_name")
    public String flower_name;

    public static List<HistoryModel> getAllHistory() {
        try {
            return new Select()
                    .from(HistoryModel.class)
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

}
