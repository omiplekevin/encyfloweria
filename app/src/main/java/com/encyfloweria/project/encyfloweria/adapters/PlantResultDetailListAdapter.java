package com.encyfloweria.project.encyfloweria.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.encyfloweria.project.encyfloweria.R;
import com.encyfloweria.project.encyfloweria.models.FlowerMetadataModel;

import java.util.List;

/**
 * Created by OMIPLEKEVIN on Jan 29, 2016.
 */
public class PlantResultDetailListAdapter extends BaseAdapter {

    private Context context;
    private FlowerMetadataModel dataModel;

    // TODO: Jan 29, 2016 change parameter after defining the Model
    public PlantResultDetailListAdapter(Context context, FlowerMetadataModel dataModel){
        this.context = context;
        this.dataModel = dataModel;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(this.context).inflate(R.layout.listview_result_detail, parent, false);

            viewHolder.detailLabel = (TextView) convertView.findViewById(R.id.fieldDetail);
            viewHolder.resultContent = (TextView) convertView.findViewById(R.id.fieldDetailContent);
            
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        switch (position) {
            case 0:
                viewHolder.detailLabel.setText("Name");
                viewHolder.resultContent.setText(dataModel.flowerName);
                break;
            case 1:
                viewHolder.detailLabel.setText("Description");
                viewHolder.resultContent.setText(dataModel.flowerDescription);
                break;
            case 2:
                viewHolder.detailLabel.setText("Scientific Name");
                viewHolder.resultContent.setText(dataModel.flowerScientificName);
                break;
            case 3:
                viewHolder.detailLabel.setText("Availability");
                viewHolder.resultContent.setText(dataModel.flowerAvailabilty);
                break;
        }
        return convertView;
    }

    class ViewHolder{
        TextView detailLabel;
        TextView resultContent;
    }
}
